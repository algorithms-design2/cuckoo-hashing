public class cuckooExam {
    static int max = 11;
    static int ver = 2;
    static int[][] hashTable = new int[ver][max];
    static int[] pos = new int[ver];

    static void initTable() {
        for (int j = 0; j < max; j++) {
            for (int i = 0; i < ver; i++) {
                hashTable[i][j] = Integer.MIN_VALUE;
            }
        }
    }

    static int hash(int function, int key) {
        if (function == 1) {
            return key % max;
        } else if (function == 2) {
            return (key / max) % max;
        }
        return Integer.MIN_VALUE;
    }

    static void place(int key, int tableID, int count, int countRecursive) {
        if (count == countRecursive) {
            System.out.printf("%d unpositioned\n", key);
            System.out.println("Cycle present. Rehash.\n");
            return;
        }
        for (int i = 0; i < ver; i++) {
            pos[i] = hash(i + 1, key);
            if (hashTable[i][pos[i]] == key) {
                return;
            }
        }

        if (hashTable[tableID][pos[tableID]] != Integer.MIN_VALUE) {
            int dis = hashTable[tableID][pos[tableID]];
            hashTable[tableID][pos[tableID]] = key;
            place(dis, (tableID + 1) % ver, count + 1, countRecursive);
        } else {
            hashTable[tableID][pos[tableID]] = key;
        }
    }

    static void printTable() {
        System.out.println("Final hash table:");

        for (int i = 0; i < ver; i++) {
            for (int j = 0; j < max; j++) {
                if (hashTable[i][j] == Integer.MIN_VALUE) {
                    System.out.print("- ");
                } else {
                    System.out.printf("%d ", hashTable[i][j]);
                }
            }
            System.out.println("");
        }
        System.out.println("");
    }

    static void cuckoo(int key[], int size) {
        initTable();

        for (int i = 0, count = 0; i < size; i++, count = 0) {
            place(key[i], 0, count, size);
        }

        printTable();
    }
    public static void main(String[] args) {
        int key_1[] = { 20, 50, 53, 75, 100, 67, 105, 3, 36, 39 };
        int num_1 = key_1.length;

        cuckoo(key_1, num_1);

        int key_2[] = { 20, 50, 53, 75, 100, 67, 105, 3, 36, 39, 6 };
        int num_2 = key_2.length;

        cuckoo(key_2, num_2);
    }
}